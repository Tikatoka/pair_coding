import time, threading
from os import system, name
import emoji
# import pygame, sys
# import pygame.locals
import time,sys,termios,tty
import curses

def clear():
        # for windows
    if name == 'nt':
        _ = system('cls')
        # for mac and linux(here, os.name is 'posix')
    else:
        _ = system('clear')


class Tamagotchi:
    def __init__(self):
        self._hunger = 50
        self._fullness = 50
        self._tiredness = 0
        self._happiness = 50
        self._age = 0
        self._coins = 100
        self.status = {
            'mood': 'happy',
            'health': 'alive'
        }

    def get_color_processbar(self, value, colorType = 1):
        bar_active = '|' * round(value / 5)
        bar_deactive = '|' * round((100 - value) / 5)
        if colorType:
            return ('\033[1;31m' + bar_active + '\033[1;37m' + bar_deactive)
        else:
            return ('\033[0;32m' + bar_active + '\033[1;37m' + bar_deactive)

    @staticmethod
    def refine_value(value):
        return min(100, max(0, value))

    def set_hunger(self, value):
        self._hunger = self.refine_value(value)

    def get_hunger(self):
        return self._hunger

    def set_fullness(self, value):
        self._fullness = self.refine_value(value)

    def get_fullness(self):
        return self._fullness

    def set_tiredness(self, value):
        self._tiredness = self.refine_value(value)

    def get_tiredness(self):
        return self._tiredness

    def set_happiness(self, value):
        self._happiness = self.refine_value(value)

    def get_happiness(self):
        return self._happiness

    def set_coins(self, value):
        self._coins = value

    def get_coins(self):
        return self._coins

    hunger = property(get_hunger, set_hunger)
    tiredness = property(get_tiredness, set_tiredness)
    happiness = property(get_happiness, set_happiness)
    fullness = property(get_fullness, set_fullness)
    coins = property(get_coins, set_coins)

    def feed(self):
        if self.coins >= 10:
            self.hunger -= 10
            self.fullness += 10
            self.coins -= 10

    def play(self):
        if self.coins >= 10:
            self.happiness += 10
            self.tiredness += 10
            self.coins -= 10

    def bed(self, timeSleep=4):
        self.tiredness -= timeSleep * 10

    def poop(self):
        if self.coins > 5:
            self.coins -= 5
            self.fullness -= 10

    def work(self):
        self.coins += 10
        self.hunger += 5
        self.fullness -= 5
        self.happiness -= 10
        self.tiredness += 20

    def timepassed(self, timepassed):
        self.inspect()
        self.tiredness += timepassed / 1
        self.hunger += timepassed / 1
        self.happiness -= timepassed / 1
        if self.hunger < 70:
            self.status['mood'] = 'happy'
        if self.hunger >= 70 and self.status['mood'] == 'happy':
            self.status['mood'] = 'hungry'
        if self.hunger >= 100:
            self.status['health'] = 'dead'
        self._age += timepassed/7

    def start(self):
        time_interval = 0.2
        th = threading.Thread(target=self.timeflying, kwargs={'time_interval': time_interval})
        th.daemon = True
        th.start()

    def timeflying(self,time_interval):
        while True:
            time.sleep(time_interval)
            self.timepassed(time_interval)

    def inspect(self):
        clear()
        if self.status['mood'] == 'happy':
            emoji_code = ':smile: [happy]'
        elif self.status['mood'] == 'hungry':
            emoji_code = ':angry: [hungry]'
        if self.status['health'] == 'dead':
            emoji_code = ':skull: [dead]'
        print(emoji.emojize('Current status: {}'.format(emoji_code), use_aliases=True), end='   ')
        print('Age:', int(self._age), 'days', end='  ')
        print('\033[1;33m Coins:', int(self.coins), emoji.emojize(":dollar:", use_aliases=True),
              '\033[0m', end='\r\n')
        print("Tiredness", self.get_color_processbar(self.tiredness, colorType=1), end='\r\n')
        print("hunger   ", self.get_color_processbar(self.hunger, colorType=1), end='\r\n')
        print("Happiness", self.get_color_processbar(self.happiness, colorType=0), end='\r\n')
        print("Fullness ", self.get_color_processbar(self.fullness, colorType=0), end='\r\n')
        print('You can take the following action to interact with your pet:\r\n'
              '[f] for feed, [p] for play, [o] for poop, [b] for bed, [w] for work and [q] for quit', end='\r\n')


if __name__ == "__main__":
    tamagotchi = Tamagotchi()
    tamagotchi.start()

    fd = sys.stdin.fileno()
    old_settings = termios.tcgetattr(fd)
    tty.setraw(fd)

    action_dict = {'p': tamagotchi.play, 'f': tamagotchi.feed,
                   'o': tamagotchi.poop, 'b': tamagotchi.bed,
                   'w': tamagotchi.work}
    try:
        while True:
            ch = sys.stdin.read(1)
            if ch in action_dict:
                action_dict[ch]()
            if ch == 'q':
                break
    finally:
        termios.tcsetattr(fd, termios.TCSANOW, old_settings)
