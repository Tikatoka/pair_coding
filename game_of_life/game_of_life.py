import time,os
import matplotlib as mpl
mpl.use('TkAgg')
from matplotlib import pyplot as plt
import matplotlib.ticker as plticker
import matplotlib.cm as cm


class GameOfLife(object):
    def __init__(self, width=10, height=10):
        self.width = width
        self.height = height
        self.map = [([0] * width)[:] for i in range(height)]
        self.generation = 0

    def input_one_position(self, position=None):
        if position is None:
            x = self.take_input_for_position('x')
            y = self.take_input_for_position('y')
        else:
            x, y = position[0], position[1]
        self.map[x][y] = 1

    def take_input_for_position(self, axis = 'x'):
        if axis == 'x':
            range_limit = self.width
        else:
            range_limit = self.height

        while True:
            position = input('Please input the {0} position of a live cell[1-{1}]:'.format(axis, range_limit))
            try:
                position = int(position)
                if 0 < position <= range_limit:
                    return position
            except:
                pass

    def visualize(self, type=1):
        if type == 1:
            os.system('clear')
            time.sleep(1)
            for row in self.map:
                for cell in row:
                    print(cell, end=' ')
                print('')
        else:
            width = self.width * 10
            height = self.height * 10
            image_data = [([254] * width)[:] for i in range(height)]

            # draw all the cells
            for x in range(self.width):
                for y in range(self.height):
                    if self.map[x][y] == 1:
                        for i in range(x*10, (x+1)*10):
                            image_data[i][(y*10):(y*10 + 10)] = [0 for i in range(10)]
            # draw the grids
            for x in range(self.width):
                for y in range(self.height + 1):
                    for i in range(x * 10, (x + 1) * 10):
                        # image_data[(x * 10):(y * 10 + 2)][y] = [128 for i in range(2)]
                        image_data[i][(y * 10):(y * 10 + 2)] = [128 for i in range(2)]
                    for i in range(10):
                        image_data[x*10][(y-1)*10+i] = 128
                        image_data[x * 10 - 1][(y - 1) * 10 + i] = 128
            for i in range(10):
                for y in range(self.height + 1):
                    image_data[x * 10 + 9][(y - 1) * 10 + i] = 128


            fig = plt.imshow(image_data, cmap=cm.Greys_r, interpolation='none')

            cid = plt.figure(1).canvas.mpl_connect('button_press_event', self.onclick)
            plt.text(self.width//2 * 10, -1, 'generation: {0}'.format(self.generation))
            # loc = plticker.MultipleLocator(base=10)
            # fig._axes.xaxis.set_major_locator(loc)
            # fig._axes.yaxis.set_major_locator(loc)
            fig.axes.get_xaxis().set_visible(False)
            fig.axes.get_yaxis().set_visible(False)
            plt.box(on=None)
            # plt.grid(True, linestyle="-", color="grey", linewidth="3")
            plt.show(block=False)
            plt.pause(1)
            plt.clf()

    def get_neighbour(self, x, y):
        return [(x-1, y-1), (x, y-1), (x+1, y-1),
                (x-1, y),             (x+1, y),
                (x-1, y+1), (x, y+1), (x+1, y+1)]

    def get_live_num_of_neighbour(self, x, y):
        neighbour = self.get_neighbour(x, y)
        live = 0
        for cell in neighbour:
            if self.live_dead_test(cell[0], cell[1]):
                live += 1
        return live

    def live_dead_test(self, x, y):
        if 0 <= x < self.width and 0 <= y < self.height:
            return self.map[x][y] == 1
        return False

    def next_generation(self):
        self.generation += 1
        nextMap = [row[:] for row in self.map]
        for x in range(self.width):
            for y in range(self.height):
                cell = self.map[x][y]
                live_neighbour_num=self.get_live_num_of_neighbour(x,y)

                if cell == 1:
                    if live_neighbour_num < 2:
                        nextMap[x][y] = 0
                    if live_neighbour_num > 3:
                        nextMap[x][y] = 0
                elif live_neighbour_num == 3:
                    nextMap[x][y] = 1
        self.map = nextMap

    def input_positions(self,start_list):
        for i in start_list:
            self.input_one_position(i)

    def onclick(self, data):
        try:
            x_in_map = int(data.xdata//10)
            y_in_map = int(data.ydata//10)
            self.map[y_in_map][x_in_map] = 0 if self.map[y_in_map][x_in_map] else 1
            self.visualize(0)
        except:
            pass



if __name__ == '__main__':
    game = GameOfLife()
    game.input_positions([(4,4),(5,4),(6,4),(7,4)])
    while True:
        # visualize 0 for GUI and 1 for command line
        game.visualize(0)
        game.next_generation()
